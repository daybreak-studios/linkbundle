<?php
	namespace DaybreakStudios\Bundle\LinkBundle\Listener;

	use DaybreakStudios\Bundle\LinkBundle\Service\ConnectionService;
	use Symfony\Component\EventDispatcher\Event;

	/**
	 * Class BootListener
	 *
	 * @package DaybreakStudios\Bundle\LinkBundle\Listener
	 *
	 * @internal this is a dummy boot listener used to ensure that the Link connection is ALWAYS booted with the
	 *           Symfony kernel.
	 */
	class BootListener {
		public function __construct(ConnectionService $cserv) {}

		public function onEvent(Event $event) {}
	}
<?php
	namespace DaybreakStudios\Bundle\LinkBundle\Command;

	use DaybreakStudios\Link\AST\DescribeManager;
	use DaybreakStudios\Link\Connection\Connection;
	use DaybreakStudios\Link\Console\Helper\ColumnTypeHelper;
	use DaybreakStudios\Link\Database\ColumnInterface;
	use DaybreakStudios\Link\Database\ForeignKeyConstraint;
	use DaybreakStudios\Link\Database\Reader\SqlReader;
	use DaybreakStudios\Link\Entity\Entity;
	use DaybreakStudios\Link\Utility\ClassBuilder\ClassBuilder;
	use DaybreakStudios\Link\Utility\ClassBuilder\Comment\BlockCommentBuilder;
	use DaybreakStudios\Link\Utility\StringUtil;
	use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
	use Symfony\Component\Console\Input\InputArgument;
	use Symfony\Component\Console\Input\InputInterface;
	use Symfony\Component\Console\Input\InputOption;
	use Symfony\Component\Console\Output\OutputInterface;
	use Symfony\Component\Console\Style\SymfonyStyle;

	class EntityGenerateCommand extends ContainerAwareCommand {
		protected function configure() {
			parent
				::setName('linkorm:entity:generate')
				->setDescription('Generates a new entity class for a table')
				->addArgument('entity', InputArgument::REQUIRED, 'The fully-qualified name of the entity class to ' .
					'create (use forward slashes instead of back slashes')
				->addOption('force', null, InputOption::VALUE_NONE, 'Forces creation of entity class, even if it ' .
					'already exists')
				->addOption('connection', null, InputOption::VALUE_REQUIRED, 'The name of the connection to use',
					'default');
		}

		protected function interact(InputInterface $input, OutputInterface $output) {
			$io = new SymfonyStyle($input, $output);

			if (!$input->getArgument('entity'))
				$input->setArgument('entity',
					$io->ask('Enter the full namespace of the entity you would like to add'));
		}

		protected function execute(InputInterface $input, OutputInterface $output) {
			$io = new SymfonyStyle($input, $output);
			$conn = Connection::getConnection($input->getOption('connection'));

			if ($conn === null)
				throw new \InvalidArgumentException('No connection found named ' . $input->getOption('connection'));

			$class = str_replace('/', '\\', $input->getArgument('entity'));
			$entity = substr($class, strrpos($class, '\\') + 1);

			$reader = new SqlReader(StringUtil::pluralize(strtolower(StringUtil::underscore($entity))), $conn);

			$relations = [];

			foreach ($reader->getConstraints() as $constraint) {
				if (!($constraint instanceof ForeignKeyConstraint))
					continue;

				$relations = array_merge($relations, array_map(function(ColumnInterface $column) {
					return $column->getField();
				}, $constraint->getColumns()));
			}

			$classBuilder = ClassBuilder::create($entity, null, substr($class, 0, strrpos($class, '\\')))
				->extend(Entity::class);

			$commentBuilder = BlockCommentBuilder::create();

			foreach ($reader->getColumns() as $column) {
				if (in_array($column->getField(), $relations))
					continue;

				$retType = ColumnTypeHelper::toPhpType($column->getType());

				$commentBuilder->addDocMethod($this->buildGetterName($column, $retType), false, [$retType]);

				if ($column->getField() === 'id')
					continue;

				$field = StringUtil::classify($column->getField());
				$commentBuilder->addDocMethod('set' . $field, false, ['$this'], [
					lcfirst($field) => ColumnTypeHelper::toPhpType($column->getType(), null),
				]);
			}

			if (sizeof($relations) > 0) {
				$io->comment('This entity has foreign keys, which means it probably relates another entity (or');
				$io->comment('entities) in your application. I cannot generate them, so you will need to add them');
				$io->comment('yourself. Please see https://goo.gl/dFvGYo for information on how to do this.');
			}

			$classBuilder->comment($commentBuilder->build());

			$path = $this->getContainer()->get('kernel')->getRootDir() . '/../src/' .
				substr($input->getArgument('entity'), 0, strrpos($input->getArgument('entity'), '/'));

			if (!file_exists($path))
				mkdir($path, 0755, true);

			if (file_exists($filePath = $path . '/' . $entity . '.php') && !$input->getOption('force'))
				throw new \Exception('File already exists at ' . $filePath);

			$result = file_put_contents($filePath, '<?php' . PHP_EOL . $classBuilder->build()->build(1));

			if ($result)
				$io->success('Generated class written to ' . realpath($filePath));
			else
				$io->error('Could not write to ' . $filePath);
		}

		/**
		 * @param ColumnInterface $column
		 * @param                 $retType
		 *
		 * @return string
		 */
		private function buildGetterName(ColumnInterface $column, $retType) {
			return ($retType === ColumnTypeHelper::TYPE_BOOLEAN ? 'is' : 'get') .
			StringUtil::classify($column->getField());
		}
	}
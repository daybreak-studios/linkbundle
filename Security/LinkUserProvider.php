<?php
	namespace DaybreakStudios\Bundle\LinkBundle\Security;

	use DaybreakStudios\Link\Entity\Entity;
	use DaybreakStudios\Link\Entity\EntityInterface;
	use DaybreakStudios\Link\Entity\EntityMetadataRegistry;
	use DaybreakStudios\Link\Utility\StringUtil;
	use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
	use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
	use Symfony\Component\Security\Core\User\UserInterface;
	use Symfony\Component\Security\Core\User\UserProviderInterface;

	class LinkUserProvider implements UserProviderInterface {
		private $userClass;
		private $property;

		/**
		 * LinkUserProvider constructor.
		 *
		 * @param string $userClass
		 * @param string $property
		 */
		public function __construct($userClass, $property) {
			if (!is_a($userClass, Entity::class, true))
				throw new \InvalidArgumentException(sprintf('%s must extend %s to be used with this provider',
					$userClass, Entity::class));

			$this->userClass = $userClass;

			$column = StringUtil::underscore($property);

			if (!EntityMetadataRegistry::get($userClass)->hasColumn($column))
				throw new \InvalidArgumentException(sprintf('%s is not a field of %s', $property, $userClass));

			$this->property = $property;
		}

		/**
		 * @return string
		 */
		public function getUserClass() {
			return $this->userClass;
		}

		/**
		 * @return string
		 */
		public function getProperty() {
			return $this->property;
		}

		/**
		 * Loads the user for the given username.
		 * This method must throw UsernameNotFoundException if the user is not
		 * found.
		 *
		 * @param string $username The username
		 *
		 * @return UserInterface
		 * @throws UsernameNotFoundException if the user is not found
		 */
		public function loadUserByUsername($username) {
			$user = call_user_func([$this->getUserClass(), 'findOneBy'], [
				$this->getProperty() => $username,
			]);

			if ($user === null)
				throw new UsernameNotFoundException(sprintf('Username %s not found', $username));

			return $user;
		}

		/**
		 * Refreshes the user for the account interface.
		 * It is up to the implementation to decide if the user data should be
		 * totally reloaded (e.g. from the database), or if the UserInterface
		 * object can just be merged into some internal array of users / identity
		 * map.
		 *
		 * @param UserInterface $user
		 *
		 * @return UserInterface
		 * @throws UnsupportedUserException if the account is not supported
		 */
		public function refreshUser(UserInterface $user) {
			$class = $this->getUserClass();

			if (!($user instanceof $class))
				throw new UnsupportedUserException(sprintf('Instances of %s are not supported', get_class($user)));

			/** @var EntityInterface $user */
			$refreshed = call_user_func([$class, 'find'], $user->getId());

			if ($refreshed === null)
				throw new UsernameNotFoundException(sprintf('User with id of %s not found', $user->getId()));

			return $refreshed;
		}

		/**
		 * Whether this provider supports the given user class.
		 *
		 * @param string $class
		 *
		 * @return bool
		 */
		public function supportsClass($class) {
			return is_a($class, $this->getUserClass(), true);
		}
	}
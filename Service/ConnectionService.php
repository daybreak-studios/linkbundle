<?php
	namespace DaybreakStudios\Bundle\LinkBundle\Service;

	use DaybreakStudios\Bundle\LinkBundle\Connection\Adapter\Adapters;
	use DaybreakStudios\Link\Connection\Connection;
	use DaybreakStudios\Link\Entity\EntitiesConfiguration;

	class ConnectionService {
		public function init($default, array $connections) {
			if (!isset($connections[$default]))
				throw new \InvalidArgumentException(sprintf('There is no connection node named %s defined', $default));

			$def = $connections[$default];

			EntitiesConfiguration::instance()
				->setConnection($this->addConnection(Adapters::getAdapterClass($def['type']), $def['options'], $default));

			foreach ($connections as $name => $connection) {
				if ($name === $default)
					continue;

				$this->addConnection(Adapters::getAdapterClass($connection['type'], true), $connection['options'],
					$name);
			}
		}

		private function addConnection($adapterKlass, array $options, $name = null) {
			return Connection::create(new $adapterKlass(
				$options['database'],
				$options['host'],
				$options['username'],
				$options['password'] ?: '',
				$options['port']
			), $name);
		}
	}
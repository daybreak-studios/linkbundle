<?php
	namespace DaybreakStudios\Bundle\LinkBundle\Request\ParamConverter;

	use DaybreakStudios\Link\Entity\Entity;
	use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
	use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
	use Symfony\Component\HttpFoundation\Request;

	class LinkEntityConverter implements ParamConverterInterface {
		public function apply(Request $request, ParamConverter $configuration) {
			$id = $request->attributes->get($configuration->getName());
			$ent = call_user_func([$configuration->getClass(), 'find'], (int)$id);

			$request->attributes->set($configuration->getName(), $ent);
		}

		public function supports(ParamConverter $configuration) {
			return is_a($configuration->getClass(), Entity::class, true);
		}
	}
<?php

	namespace DaybreakStudios\Bundle\LinkBundle;

	use DaybreakStudios\Bundle\LinkBundle\DependencyInjection\DaybreakStudiosLinkExtension;
	use Symfony\Component\HttpKernel\Bundle\Bundle;

	class DaybreakStudiosLinkBundle extends Bundle {
		public function getContainerExtension() {
			return new DaybreakStudiosLinkExtension();
		}
	}

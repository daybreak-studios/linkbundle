<?php

	namespace DaybreakStudios\Bundle\LinkBundle\DependencyInjection;

	use Symfony\Component\Config\Definition\Builder\TreeBuilder;
	use Symfony\Component\Config\Definition\ConfigurationInterface;

	/**
	 * This is the class that validates and merges configuration from your app/config files.
	 *
	 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
	 */
	class Configuration implements ConfigurationInterface {
		/**
		 * {@inheritdoc}
		 */
		public function getConfigTreeBuilder() {
			$treeBuilder = new TreeBuilder();
			$rootNode = $treeBuilder->root('linkorm');

			$rootNode
				->children()
					->scalarNode('default')
						->cannotBeEmpty()
						->defaultValue('default')
						->end()
					->arrayNode('connections')
						->requiresAtLeastOneElement()
						->prototype('array')
							->children()
								->scalarNode('type')
									->cannotBeEmpty()
									->end()
								->arrayNode('options')
									->children()
										->scalarNode('database')
											->cannotBeEmpty()
											->end()
										->scalarNode('host')
											->cannotBeEmpty()
											->end()
										->scalarNode('username')
											->cannotBeEmpty()
											->end()
										->scalarNode('password')
											->end()
										->scalarNode('port')
											->defaultNull()
											->end();

			return $treeBuilder;
		}
	}

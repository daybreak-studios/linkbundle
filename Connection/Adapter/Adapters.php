<?php
	namespace DaybreakStudios\Bundle\LinkBundle\Connection\Adapter;

	use DaybreakStudios\Link\Connection\Adapter\MySqlPdoAdapter;
	use DaybreakStudios\Link\Connection\Adapter\PostgreSqlPdoAdapter;

	class Adapters {
		const PDO_MYSQL = 'pdo.mysql';
		const PDO_PGSQL = 'pdo.pgsql';

		const ADAPTERS = [
			self::PDO_MYSQL => MySqlPdoAdapter::class,
			self::PDO_PGSQL => PostgreSqlPdoAdapter::class,
		];

		/**
		 * @param string $type
		 * @param bool   $throwOnNotFound
		 *
		 * @return null|string
		 */
		public static function getAdapterClass($type, $throwOnNotFound = false) {
			if (!array_key_exists($type, self::ADAPTERS)) {
				if ($throwOnNotFound)
					throw new \InvalidArgumentException(sprintf('No adapter found matching %s', $type));

				return null;
			}

			return self::ADAPTERS[$type];
		}
	}